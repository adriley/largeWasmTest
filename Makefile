CC = emcc
CFLAGS = -O2

pages:
	@mkdir -p public/hello/file
	@mkdir -p public/hello/sdl
	@cp index.html public/
	@${CC} ${CFLAGS} tests/hello.cpp -o public/hello/index.html -s TOTAL_MEMORY=256MB -s ALLOW_MEMORY_GROWTH

